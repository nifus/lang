<?php

class CDictionary extends BaseController {

	public function index(){
		return Response::json( Dictionary::getOrderList() );
	}
    public function search(){
        $query = \Input::get('query');
		return Response::json( Dictionary::getSearchList($query) );
	}

    public function store(){
        $dictionary = Dictionary::create( \Input::all() );
        $id = $dictionary->id;
        return Response::json( ['success'=>true,'id'=>$id] );
    }

    public function update($id){
        Dictionary::find($id)->update(\Input::all());
        return Response::json( ['success'=>true,'id'=>$id] );
    }

    public function destroy($id){
        Dictionary::find($id)->delete();
        return Response::json( ['success'=>true] );
    }
    public function plus($id){
        $item = Dictionary::find($id);
            $item->update(['sort'=>$item->sort+1 ]);
        return Response::json( ['success'=>true] );
    }

}
