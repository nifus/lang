<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypeDictionary extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("dictionary", function($table)
		{
		    $table->enum("type",['question','approval','negation','time'])->default('approval');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("dictionary", function($table)
		{
		    $table->dropColumn("type");
		});
	}

}
