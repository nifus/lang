<?php


class Dictionary extends \Eloquent
{

    protected
        $table = 'dictionary',
        $fillable = ['en','ru','type','sort','tense'],

    $primaryKey = 'id';

    public $timestamps = false;



    static function getSearchList($query){
        return self::where('en','like','%'.$query.'%')->orWhere('ru','like','%'.$query.'%')->orderBy('id','DESC')->get()->toArray();
    }
    static function getOrderList(){
        return self::orderBy('sort','DESC')->orderBy('id','DESC')->get()->toArray();
    }



}