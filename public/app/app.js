var LangApp = angular.module('LangApp', [
    'ngRoute'
]).config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.

            when('/', {
                templateUrl: '/app/views/controllers/dashboard/index.html',
                controller: 'Dashboard'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);

/**
 * Загружаем скопом данные о юзере
 */
LangApp.factory('DictionaryDataLoader', function ($http) {
    return function ( callback) {
        $http.get('/dictionary').success(function (data) {
            callback(data)
        });
    }
});
