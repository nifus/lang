
LangApp.controller('Dashboard', function ($scope,$http,DictionaryDataLoader){
    $scope.env = {
        dictionary:undefined,
        edit_id:null,
        search:null
    };
    $scope.model = {
        en:null,
        ru:null,
        type:'approval',
        tense:'present'
    };
    $scope.actions = {


        plus:function(id){
            $http.post('/dictionary/'+id+'/plus', {}).success(function (data) {
                if (data.success == true) {
                    $scope.actions.loadDictionary();
                    $scope.actions.resetForm();
                }
            });
        },
        under:function(){
            $scope.env.edit_id = null;
            $scope.actions.resetForm()
        },
        getById:function(id){
            return $scope.env.dictionary.filter( function(row){
                if ( row.id==id ){
                    return row;
                }
            })[0];
        },
        loadDictionary:function(){
            DictionaryDataLoader(function (data) {
                $scope.env.dictionary=  data;
            })
        },
        resetForm:function(){
            $scope.model = {
                en:null,
                ru:null,
                type:'approval',
                tense:'present'
            };
        },
        saveForm:function(id){
            var send = angular.copy($scope.model);

            if  (id == null ){
                $http.post('/dictionary', send).success(function (data) {
                    if (data.success == true) {
                        $scope.actions.loadDictionary();
                        $scope.actions.resetForm();
                    }
                });
            }else{
                $http.put('/dictionary/'+id, send).success(function (data) {
                    if (data.success == true) {
                        $scope.actions.loadDictionary();
                        $scope.actions.resetForm();
                        $scope.env.edit_id = null;
                    }
                });

            }

        },
        editForm:function(id){
            $scope.env.edit_id = id;
            var row = $scope.actions.getById(id);
            $scope.model = angular.copy(row);
        },
        delete:function(id){
            $http.delete('/dictionary/'+id, []).success(function (data) {
                if (data.success == true) {
                    $scope.actions.loadDictionary();
                    $scope.env.edit_id = null;
                    $scope.actions.resetForm();
                }
            });
        }
    }
    $scope.actions.loadDictionary();
    $scope.$watch('env.search',function(value){
        if ( value==null || value==undefined ){
            return;
        }
        $http.post('/dictionary/search', {query:value} ).success(function (data) {
            $scope.env.dictionary = data;
        });
    })

});
